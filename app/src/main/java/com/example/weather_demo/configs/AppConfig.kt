package com.example.weather_demo.configs

class AppConfig {
    companion object {
        const val API_URL = "https://api.openweathermap.org/data"
        const val API_VERSION = 2.5
        const val API_KEY = "201c09f949ee8912688bee36d4d242a6"
    }
}