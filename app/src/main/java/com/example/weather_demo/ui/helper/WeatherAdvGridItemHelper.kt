package com.example.weather_demo.ui.helper

import android.app.Activity
import android.content.Context
import android.widget.LinearLayout
import android.widget.TextView
import com.example.weather_demo.R
import com.example.weather_demo.models.structures.WeatherAdvanced


class WeatherAdvGridItemHelper {
    companion object {
        fun createLinearLayoutItem(
            context: Context,
            weather_adv_item: WeatherAdvanced
        ) : LinearLayout {
            val lLayout = (context as Activity).layoutInflater.inflate(
                R.layout.view_weather_item, null, false
            ) as LinearLayout

            (lLayout.getChildAt(0) as TextView).text =  weather_adv_item.name;
            (lLayout.getChildAt(1) as TextView).text =  weather_adv_item.value;

            return lLayout
        }
    }
}