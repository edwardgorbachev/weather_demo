package com.example.weather_demo.ui.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.weather_demo.R
import com.example.weather_demo.configs.AppConfig
import com.example.weather_demo.models.api.responses.Weather
import com.example.weather_demo.ui.helper.WeatherAdvGridItemHelper
import com.example.weather_demo.ui.helper.WeatherIconHelper
import java.net.URL


class MainActivity : AppCompatActivity() {

    var city = "Москва"
    var cityPrev = city
    var units: String = "metric" // standart|metric|imperial
    var lang = "ru" // ru|en etc

    private lateinit var tvMyPlace: TextView
    private lateinit var tvChangeCity: TextView
    private lateinit var ivMyPlace: ImageView

    private lateinit var tvCityName: TextView
    private lateinit var cLayoutWeatherNav: ConstraintLayout
    private lateinit var cLayoutChooseCity: ConstraintLayout
    private lateinit var btCityNameOk: TextView
    private lateinit var pbLoader: ProgressBar
    private lateinit var gLayoutWeather: GridLayout
    private lateinit var etCityName: EditText
    private var locationManager: LocationManager? = null

    private var userLocation: Location? = null

    private var blockUI = false

    private lateinit var rgUnits: RadioGroup

    enum class Units (val code: String) {
        METRIC ("metric"),
        IMPERIAL ("imperial")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        initViews()
        weatherTask().execute()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    private fun initViews() {
        cLayoutWeatherNav = this.findViewById(R.id.layout_weather_nav)
        cLayoutChooseCity = this.findViewById(R.id.layout_choose_city)
        tvMyPlace = this.findViewById(R.id.tv_my_place)
        ivMyPlace = this.findViewById(R.id.iv_my_place)
        tvCityName = this.findViewById(R.id.tv_city_name)
        btCityNameOk = this.findViewById(R.id.bt_city_name_ok)
        pbLoader = this.findViewById(R.id.pb_loader)
        tvChangeCity = this.findViewById(R.id.tv_change_city)
        gLayoutWeather = this.findViewById(R.id.grid_weather_stats)
        rgUnits = this.findViewById(R.id.rg_units)
        etCityName = this.findViewById(R.id.et_city_name)

        tvCityName.text = city

        tvChangeCity.setOnClickListener {
            cLayoutWeatherNav.visibility = View.GONE
            cLayoutChooseCity.visibility = View.VISIBLE
        }

        btCityNameOk.setOnClickListener {
            cLayoutWeatherNav.visibility = View.VISIBLE
            cLayoutChooseCity.visibility = View.GONE

            setCityValueFromInput()
            userLocation = null
            hideKeyboard()

            weatherTask().execute()
        }

        rgUnits.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_units_metric -> {
                    if (units != Units.METRIC.code) {
                        units = Units.METRIC.code
                        weatherTask().execute()
                    }
                }
                R.id.rb_units_imperial -> {
                    if (units != Units.IMPERIAL.code) {
                        units = Units.IMPERIAL.code
                        weatherTask().execute()
                    }
                }
            }
        }
        // units

        tvMyPlace.setOnClickListener {
            getCurrentLocation()
        }

        ivMyPlace.setOnClickListener {
            getCurrentLocation()
        }
    }

    private fun hideKeyboard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etCityName.windowToken, 0)
    }

    private fun getCurrentLocation() {
        if ( checkPermissions() ){
            try {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                showLoader()
                locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
            } catch (e: Exception) {
                hideLoader()
            }

        }
    }

    private fun checkPermissions(): Boolean {
        val listPermissionsNeeded = ArrayList<String>()

        val permissionFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)

        if (permissionFineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }

        if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toArray(arrayOfNulls<String>(listPermissionsNeeded.size)),
                0
            )

            return false
        }

        return true
    }

    private fun showLoader() {
        pbLoader.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        pbLoader.visibility = View.GONE
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            userLocation = location
            hideLoader()

            locationManager?.removeUpdates(this)

            weatherTask().execute()
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            hideLoader()
        }
        override fun onProviderEnabled(provider: String) {
            hideLoader()
        }
        override fun onProviderDisabled(provider: String) {
            showToast("Распознавание локации отключено")
            hideLoader()
        }
    }

    inner class weatherTask() : AsyncTask<String, Void, String>() {

        override fun onPreExecute() {
            super.onPreExecute()

//            if (!blockUI) {
//
//            }
            showLoader()
        }

        override fun doInBackground(vararg params: String?): String? {
            var response: String?
            try {
                val baseApiUrl = AppConfig.API_URL
                val apiVersion = AppConfig.API_VERSION
                val apiKey = AppConfig.API_KEY


                // todo better realize by retrofit
                var userLocationParams = ""
                var q = ""

                if (userLocation != null) {
                    userLocationParams = "&lat=${userLocation!!.latitude}&lon=${userLocation!!.longitude}"
                } else {
                    q = "&q=$city"
                }

                response =
                    URL("$baseApiUrl/$apiVersion/weather?" +
                            "units=$units" +
                            q +
                            "&lang=$lang" +
                            "&appid=$apiKey"+
                            userLocationParams).readText(
                        Charsets.UTF_8
                    )
            } catch (e: Exception) {
                response = null
            }
            return response
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                val weather = Weather()
                weather.parseResponse(this@MainActivity, result!!)
                //weather.parseResponse(result!!)

                findViewById<TextView>(R.id.tv_weather_temp).text = weather.tempText
                findViewById<TextView>(R.id.tv_weather_text).text = weather.weatherDescription

                val drawableId = WeatherIconHelper.getDrawableIdByWeatherCode(weather.weatherCode!!)

                findViewById<ImageView>(R.id.iv_weather_icon).setImageDrawable(
                    resources.getDrawable(drawableId, null)
                )

                gLayoutWeather.removeAllViews()

                for (weatherItem in weather.advancedDetails) {
                    val lLayout = WeatherAdvGridItemHelper.createLinearLayoutItem(
                            this@MainActivity, weatherItem
                    )

                    gLayoutWeather.addView(lLayout)
                }

                for (i in 0 until gLayoutWeather.childCount) {
                    val child = gLayoutWeather.getChildAt(i)

                    val param: GridLayout.LayoutParams = GridLayout.LayoutParams(
                        GridLayout.spec(
                            GridLayout.UNDEFINED, GridLayout.FILL, 1f
                        ),
                        GridLayout.spec(GridLayout.UNDEFINED, GridLayout.FILL, 1f)
                    )

                    child.layoutParams = param
                }

                setCityValue(weather.cityName)
                hideLoader()
            } catch (e: Exception) {
                hideLoader()
                redoCityValue()
                showToast("Город не найден")
            }
        }
    }

    private fun setCityValueFromInput() {
        cityPrev = city
        city = etCityName.text.toString()
        tvCityName.text = city
    }

    private fun setCityValue(newCity: String) {
        cityPrev = city
        city = newCity
        tvCityName.text = city
    }

    private fun redoCityValue() {
        city = cityPrev
        tvCityName.text = city
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}