package com.example.weather_demo.ui.helper

import com.example.weather_demo.R


class WeatherIconHelper {
    companion object {
        fun getDrawableIdByWeatherCode(
            weather_code: Int
        ): Int {
            return when (weather_code)  {
                in 200..299 -> R.drawable.ic_storm
                in 300..399 -> R.drawable.ic_rain
                in 500..599 -> R.drawable.ic_rain
                in 600..699 -> R.drawable.ic_cloud
                in 700..799 -> R.drawable.ic_cloud
                800 -> R.drawable.ic_sun
                in 801..803 -> R.drawable.ic_partly_cloudy
                804 -> R.drawable.ic_cloud
                else -> R.drawable.ic_cloud
            }
        }
    }
}