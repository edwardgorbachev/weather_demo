package com.example.weather_demo.models.api.responses

import android.content.Context
import android.util.Log
import com.example.weather_demo.R
import com.example.weather_demo.models.structures.WeatherAdvanced
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class Weather {
    var tempValue: Int? = null
    var tempText: String? = null

    var weatherDescription: String? = null

    //
    var weatherMain: String? = null

    var probabilityPrecipitation : Int? = null
    var humidity : Int? = null

    var cityName: String = ""

    var weatherCode: Int? = null

    var windDescription: String? = null
    var pressureDescription: String? = null
    var humidityDescription: String? = null
    var probabilityPrecipitationDescription: String? = null

    val advancedDetails = arrayListOf<WeatherAdvanced>()

    lateinit var windDirections: Array<String>

    fun parseResponse(context: Context, response: String) {
        windDirections = context.resources.getStringArray(R.array.wind_direction_texts)

//        windDirections = arrayOf(
//            "N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"
//        )

        try {
            val jsonObj = JSONObject(response)
            val main = jsonObj.getJSONObject("main")
            val sys = jsonObj.getJSONObject("sys")
            val wind = jsonObj.getJSONObject("wind")
            val weather = jsonObj.getJSONArray("weather").getJSONObject(0)

            cityName = jsonObj.getString("name")

            val updatedAt: Long = jsonObj.getLong("dt")
            val updatedAtText =
                    "Updated at: " + SimpleDateFormat("dd/mm/yyyy hh:mm a", Locale.ENGLISH)
                            .format(
                                    Date(updatedAt * 1000)
                            )

            weatherDescription = weather.getString("description").capitalize()
            weatherCode = weather.getInt("id")
            weatherMain = weather.getString("main")

            val tempVal = main.getString("temp").toDouble().roundToInt()
            tempValue = tempVal
            tempText = "$tempVal°"

            val pressure = main.getString("pressure")
            val humidity = main.getString("humidity")

            val windSpeed = wind.getString("speed")
            val windDeg = wind.getDouble("deg")

            val windText = getWindDirectionByDeg(windDeg)

            windDescription = windSpeed + "м/c, $windText" // todo to strings placeholder
            pressureDescription = "$pressure мм рт. ст." // todo to strings placeholder
            humidityDescription = "$humidity%" // todo to strings placeholder
            //probabilityPrecipitationDescription = "10%" // todo to strings and placeholder
            probabilityPrecipitationDescription = "N/A" // todo to strings and placeholder

            advancedDetails.add(
                WeatherAdvanced(
                    context.getString(R.string.advanced_params_wind),
                    windDescription!!
                )
            )

            advancedDetails.add(
                WeatherAdvanced(
                    context.getString(R.string.advanced_params_pressure),
                    pressureDescription!!
                )
            )

            advancedDetails.add(
                WeatherAdvanced(
                    context.getString(R.string.advanced_params_humidity),
                    humidityDescription!!
                )
            )

            advancedDetails.add(
                WeatherAdvanced(
                    context.getString(R.string.advanced_params_probability_precipitation),
                    probabilityPrecipitationDescription!!
                )
            )
        }    catch (e: Exception) {
            Log.d("weather_logs", "error catch")
            Log.d("weather_logs", "$e")
            Log.d("weather_logs", "${e.message}")
            throw e
        }
    }

    private fun getWindDirectionByDeg(deg: Double): String {
        //-337.5 - 22.5 - север 0
        // 22.5  - 67.5 - северо-восток 45
        // 67.5  - 112.5 - восток 90
        // 112.5 - 157.5 - юго-восток 135
        // 157.5 - 202.5 - юг 180
        // 202.5 - 247.5 - юг-запад 225
        // 247.5 - 292.5 - запад 270
        // 292.5 - 337.5 - северо-запад 315

        val degInt = ((deg / 22.5) + 0.5).toInt()

        return windDirections[degInt % 16]
    }
}