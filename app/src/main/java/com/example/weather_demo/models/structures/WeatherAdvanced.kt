package com.example.weather_demo.models.structures

data class WeatherAdvanced(val name: String, val value: String)